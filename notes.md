**Atelier IPv6 aux JDLL 2015**

__Texte envoyé pour le site__

IPv6 a longtemps été le « protocole du futur » d'Internet.  C'est maintenant le protocole du présent : nombre d'hébergeurs et de fournisseurs d'accès à Internet proposent désormais IPv6 comme service standard.  Cet atelier a pour but d'apprendre à utiliser IPv6 par la pratique, afin de dédramatiser son usage.

La première partie (30 minutes) est consacrée aux simples utilisateurs : intérêt d'IPv6, principes de base, comment l'activer chez soi, manipulations (DNS, navigateur, traceroute).

La seconde partie (1 heure) est destinée aux « administrateurs système amateurs » qui gèrent un serveur, éventuellement auto-hébergé.  Nous verrons les étapes nécessaires, de A à Z, pour proposer des services en IPv6 : routage, configuration DNS, firewall, configuration des logiciels (serveur web, serveur mail).  L'atelier sera basé sur une distribution Linux de type Debian, avec des logiciels libres (iptables, bind, postfix, nginx). 


__Demande de matériel à l'orga__

Vidéo-projecteur nécessaire, ainsi qu'un accès IPv6 à Internet pour les participants à l'atelier.

Il serait nécessaire d'avoir des machines sous Debian wheezy, avec accès root (configuration des services).  Les paquets souhaités sont : nginx-full, bind9, postfix, iptables, dnsutils, ndisc6, traceroute, netcat-openbsd, iputils-ping.  Une Debian live pourrait suffire, si il y a possibilité d'installer les paquets sus-mentionnés. 


__Notes random__

Première partie, utilisateur d'IPv6 : intérêt, à quoi ressemble une IPv6 (adresse, subnet, notation, taille), activer IPv6 chez soi (SFR, Free, pas Orange), comment manipuler IPv6 (extension firefox, ping/ping6, traceroute/traceroute6, dig)

Seconde partie, « administrateur système amateur » (potentiellement auto-hébergé) : serveur web, mail, DNS.  Un peu de pratique (pas forcément évident si on n'est pas root), mais surtout les grandes lignes : listen address, firewall, reverse DNS pour le mail, enregistrements AAAA, etc.

Il serait intéressant d'avoir accès à une interface type Gandi/OVH/Bookmyname pour vraiment pratiquer les enregistrements DNS.

Besoins : vidéo-projecteur, machines sous Linux.  Si possible pour la deuxième partie, pouvoir être root. 


__Première partie__ (utilisateurs)

Support : slides au début

  * Intro très générale (historique, IPv4, manque d'adresses, nécessité de connecter plein d'appareils, NAT, IPv6)
  * Déploiement d'IPv6 en France (FAI, hébergeurs)
  * Addresses IPv6 (notation, taille, subnet, différence par rapport à IPv4)
  * Sécurité
  * Comment activer IPv6 chez soi (Free, SFR)

Manipulation :
  * Addon Firefox : SixOrNot, pour voir si un site est disponible en IPv6 ou non (tester avec Google, www.lemonde.fr )
  * Résolution DNS (host, dig)
  * ping/ping6, traceroute/traceroute6
  * 


Deuxième partie (Administrateurs)
  * iptables : utilitaire ip6tables, meme fonctionnement générale qu'en ipv4, mais on oublie le NAT !
  * nginx
    * Penser à écouter sur l'IPv6
    * Spécifier les enregistrements AAAA
    * Configurer le pare-feu
  * postfix
    * Spécifier l'utilisation d'IPv6 si disponible :inet_protocols = all
    * Configurer l'adresse IPv6 de sortie : smtp_bind_address6
    * Penser à adapter mynetworks
    * Spécifier un enregistrement AAAA au niveau du serveur DNS
    * Configurer le pare-feu
    * Vérifier les champs SPF et MX
  * bind (J'ai un domaine pas encore utilisé si on veut jouer : unicorntelecom.eu)
    * Écouter en v6 : listen-on-v6
    * Penser à adapter de potentielles ACL

DNS : déléguer un sous-domaine par participant (bidule.polyno.me) directement à sa machine, via un NS.  Possible car IPv6.

